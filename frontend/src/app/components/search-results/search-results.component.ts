import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Subscription} from 'rxjs';
import {Result} from '../../models/result';
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  @ViewChild('tabSet', {static: false}) tabSet: NgbTabset;
  hasResults = false;
  private subscription: Subscription;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.hasResults = this.dataService.getResults().length > 0;
    this.subscription = this.dataService.resultsList$.subscribe(
      results => this.hasResults = (results.length > 0)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onNavigateClicked(result: Result) {
    this.dataService.runNavigationQuery(result);
    this.tabSet.select('map-results');
  }
}
