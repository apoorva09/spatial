import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  @Input() rating: number;

  constructor() { }

  ngOnInit(): void {
  }

  getFullStars(): number {
    return Math.floor(this.rating);
  }

  hasHalfStar(): boolean {
    return (this.rating * 10) % 10 > 0;
  }

  getEmptyStars(): number {
    return 5 - Math.ceil(this.rating);
  }

}
