import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'distance'
})
export class DistancePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if ((value as number) * 5280 >= 1000) {
      return ((value as number).toFixed(1)).toString() + ' Mi';
    } else if (value != null) {
      // Compute in feet rounded to 100 ft chunks.
      const feet = (value as number) * 5280;
      return (Math.ceil(feet / 100) * 100).toString() + ' Ft';
    } else {
      return null;
    }
  }
}
