import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultInListComponent } from './result-in-list.component';

describe('ResultInListComponent', () => {
  let component: ResultInListComponent;
  let fixture: ComponentFixture<ResultInListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultInListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultInListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
