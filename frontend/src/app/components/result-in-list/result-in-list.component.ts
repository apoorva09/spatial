import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Result} from '../../models/result';

@Component({
  selector: 'app-result-in-list',
  templateUrl: './result-in-list.component.html',
  styleUrls: ['./result-in-list.component.css']
})
export class ResultInListComponent implements OnInit {

  @Input() result: Result;
  @Output() navigateClicked: EventEmitter<Result> = new EventEmitter<Result>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onNavigateClick() {
    this.navigateClicked.emit(this.result);
  }
}
