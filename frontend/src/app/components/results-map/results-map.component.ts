import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {Result} from '../../models/result';
import {DataService} from '../../services/data.service';
import {MapLocation} from '../../models/map-location';
import {RoadNode} from '../../models/road-node';

@Component({
  selector: 'app-results-map',
  templateUrl: './results-map.component.html',
  styleUrls: ['./results-map.component.css']
})
export class ResultsMapComponent implements OnInit, OnDestroy {

  @Output() navigateResult = new EventEmitter<Result>();

  private resultSubscription: Subscription;
  results: Result[];

  private pathSubscription: Subscription;
  path: RoadNode[];

  private queryLocationSubscription: Subscription;
  queryLocation: MapLocation;
  currentPin: number = null;

  constructor(private dataService: DataService, private cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.results = this.dataService.getResults();
    this.resultSubscription = this.dataService.resultsList$.subscribe(
      results => {
        this.results = results;
        this.currentPin = null;
      }
    );
    this.queryLocation = this.dataService.getQueryLocation();
    this.queryLocationSubscription = this.dataService.queryLocation$.subscribe(
      location => {
        this.queryLocation = location;
        this.cdRef.detectChanges();
      }
    );
    this.path = this.dataService.getPath();
    this.pathSubscription = this.dataService.path$.subscribe(path => {
      this.path = path;
      this.cdRef.detectChanges();
    });
  }

  onNavigateClick(result: Result) {
    this.navigateResult.emit(result);
  }

  ngOnDestroy(): void {
    this.resultSubscription.unsubscribe();
    this.queryLocationSubscription.unsubscribe();
    this.pathSubscription.unsubscribe();
  }
}
