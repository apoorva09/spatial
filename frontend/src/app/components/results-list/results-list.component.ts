import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {Result} from '../../models/result';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-results-list',
  templateUrl: './results-list.component.html',
  styleUrls: ['./results-list.component.css']
})
export class ResultsListComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  results: Result[];
  @Output() navigateClicked = new EventEmitter<Result>();

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.results = this.dataService.getResults();
    this.subscription = this.dataService.resultsList$.subscribe(
      results => this.results = results
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onNavigateClicked(result: Result) {
    this.navigateClicked.emit(result);
  }
}
