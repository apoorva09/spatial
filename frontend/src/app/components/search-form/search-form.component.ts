import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SelectLocationModalComponent} from '../select-location-modal/select-location-modal.component';
import {Query, SortingMethod} from '../../models/query';
import {MapLocation} from '../../models/map-location';
import {DataService} from '../../services/data.service';
import {Observable, of} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  @ViewChild('selectLocationModal', {static: false}) locationSelectModal: SelectLocationModalComponent;
  validatingForm: FormGroup;
  sortingMethod: string;

  searchingCategory = false;
  categorySearchFailed = false;
  categories: string[] = ['one', 'two'];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.validatingForm = new FormGroup({
      latitude: new FormControl(36.115021),
      longitude: new FormControl(-115.172508),
      name: new FormControl(''),
      category: new FormControl(''),
      stars: new FormControl(0.0),  // Default to no-rating selection.
      oneDollars: new FormControl(true),
      twoDollars: new FormControl(true),
      threeDollars: new FormControl(true),
      fourDollars: new FormControl(true),
      fiveDollars: new FormControl(true),
      takeOut: new FormControl(false),
      wifi: new FormControl(false),
      gfGroups: new FormControl(false),
      distance: new FormControl(25),
    });
    this.dataService.fetchCategories((results) => this.categories = results);
  }

  get latitude() {
    return this.validatingForm.get('latitude');
  }

  get longitude() {
    return this.validatingForm.get('longitude');
  }

  get name() {
    return this.validatingForm.get('name');
  }

  get category() {
    return this.validatingForm.get('category');
  }

  get stars() {
    return this.validatingForm.get('stars');
  }

  get distance() {
    return this.validatingForm.get('distance');
  }

  get oneDollars() {
    return this.validatingForm.get('oneDollars');
  }
  get twoDollars() {
    return this.validatingForm.get('twoDollars');
  }
  get threeDollars() {
    return this.validatingForm.get('threeDollars');
  }
  get fourDollars() {
    return this.validatingForm.get('fourDollars');
  }
  get fiveDollars() {
    return this.validatingForm.get('fiveDollars');
  }

  get takeOut() {
    return this.validatingForm.get('takeOut');
  }
  get wifi() {
    return this.validatingForm.get('wifi');
  }
  get gfGroups() {
    return this.validatingForm.get('gfGroups');
  }

  onSearchButtonClick(): void {
    this.validatingForm.disable();
    // Construct the query object based on the inputs from the user.
    const query = new Query();
    if (this.getVal('latitude') != null && this.getVal('longitude') != null) {
      query.location = new MapLocation(parseFloat(this.getVal('latitude')), parseFloat(this.getVal('longitude')));
    }
    if (this.getVal('name') != null && this.getVal('name') !== '') {
      query.nameTokens = (this.getVal('name') as string).split(/\W+/);
    }
    if (this.getVal('stars') >= 0.4) {
      query.minRating = parseFloat(this.getVal('stars'));
    }
    if (this.getVal('takeOut') === true) {
      query.takeOut = true;
    }
    if (this.getVal('wifi') === true) {
      query.wifi = true;
    }
    if (this.getVal('gfGroups') === true) {
      query.goodForGroups = true;
    }
    query.priceRanges = this._getSelectedPriceRanges();
    query.sortBy = this.sortingMethod as SortingMethod;
    if (this.getVal('distance') !== 25) {
      // Distance slider was adjusted.
      query.maxDistance = this.getVal('distance');
    }
    if (this.getVal('category')) {
      query.category = (this.getVal('category') as string).trim();
    }
    this.dataService.runSearchQuery(query, () => this.validatingForm.enable());
  }

  private _getSelectedPriceRanges(): number[] {
    const ranges: number[] = [];
    if (this.getVal('oneDollars') === true) {
      ranges.push(1);
    }
    if (this.getVal('twoDollars') === true) {
      ranges.push(2);
    }
    if (this.getVal('threeDollars') === true) {
      ranges.push(3);
    }
    if (this.getVal('fourDollars') === true) {
      ranges.push(4);
    }
    if (this.getVal('fiveDollars') === true) {
      ranges.push(5);
    }

    if (ranges.length === 5) {
      // Everything selected == no preference of the user.
      return null;
    }
    return ranges;
  }

  onSelectLocationClick(): void {
    this.locationSelectModal.setStartLatLong(this.validatingForm.get('latitude').value,
      this.validatingForm.get('longitude').value);
    this.locationSelectModal.openDialog();
  }

  onLocationSelected(coords): void {
    this.validatingForm.get('latitude').setValue(coords.lat);
    this.validatingForm.get('longitude').setValue(coords.lng);
  }

  private getVal(controlName): any {
    return this.validatingForm.get(controlName).value;
  }

  private _searchCategories(term: string): string[] {
    return this.categories
      .map(cat => ({category: cat, pos: cat.toLowerCase().indexOf(term.toLowerCase())}))
      .filter(tuple => tuple.pos >= 0)
      .sort((a, b) => a.pos - b.pos)
      .map(tuple => tuple.category);
  }

  categorySearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      tap(() => this.searchingCategory = true),
      switchMap(term => of(this._searchCategories(term))),
      tap(() => this.searchingCategory = false))
}
