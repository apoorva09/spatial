import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLocationModalComponent } from './select-location-modal.component';

describe('SelectLocationModalComponent', () => {
  let component: SelectLocationModalComponent;
  let fixture: ComponentFixture<SelectLocationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLocationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLocationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
