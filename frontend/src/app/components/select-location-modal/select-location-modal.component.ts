import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-select-location-modal',
  templateUrl: './select-location-modal.component.html',
  styleUrls: ['./select-location-modal.component.css']
})
export class SelectLocationModalComponent implements OnInit {

  @Output() locationSelected = new EventEmitter();
  @ViewChild('frame', {static: false}) modalDialog: ModalDirective;

  startingLat: number;
  startingLong: number;
  selectedLat: number;
  selectedLong: number;

  constructor() { }

  ngOnInit(): void {
  }

  public setStartLatLong(lat, long) {
    this.startingLat = lat;
    this.startingLong = long;
    this.selectedLat = lat;
    this.selectedLong = long;
  }

  public openDialog() {
    this.modalDialog.show();
  }

  public closeDialog() {
    this.modalDialog.hide();
  }

  onMarkerDragEnd(event) {
    this.selectedLat = event.coords.lat;
    this.selectedLong = event.coords.lng;
  }

  onLocationSelected() {
    this.locationSelected.emit({lat: this.selectedLat, lng: this.selectedLong});
    this.closeDialog();
  }
}
