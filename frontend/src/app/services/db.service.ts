import {Injectable} from '@angular/core';
import neo4j, {QueryResult} from 'neo4j-driver';
import {Business} from '../models/business';
import {Query, SortingMethod} from '../models/query';
import {Result} from '../models/result';
import {MapLocation} from '../models/map-location';
import {forkJoin} from 'rxjs';
import {RoadNode} from '../models/road-node';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  private driver: any;

  constructor() {
    const boltUrl = 'bolt://ec2-18-144-71-227.us-west-1.compute.amazonaws.com';
    const userName = 'neo4j';
    const password = 'i-0f6a4924d99e60336';
    this.driver = neo4j.driver(boltUrl, neo4j.auth.basic(userName, password));
  }


  static _constructQuery(q: Query): string {
    const conditions: string[] = [];
    // Name tokens.
    if (q.nameTokens && q.nameTokens.length > 0) {
      q.nameTokens.forEach(token => {
        conditions.push('b.name =~ \'(?i).*' + token + '.*\'');
      });
    }
    // Price-Ranges.
    if (q.priceRanges && q.priceRanges.length > 0) {
      const clauses = [];
      q.priceRanges.forEach(range => clauses.push('b.restaurantPriceRange = \'' + range + '\''));
      conditions.push('(' + clauses.join(' OR ') + ')');
    }
    // Min-rating.
    if (q.minRating) {
      conditions.push('b.stars >= ' + q.minRating);
    }
    // Max distance.
    if (q.maxDistance) {
      conditions.push('Distance <= ' + q.maxDistance);
    }
    // Take-out.
    if (q.takeOut) {
      conditions.push('b.takeOut = \'True\'');
    }
    // WiFi.
    if (q.wifi) {
      conditions.push('b.wifi = \"u\'free\'\"');
    }
    // Good for groups.
    if (q.goodForGroups) {
      conditions.push('b.restaurantGoodForGroups = \'True\'');
    }
    // Category.
    if (q.category && q.category.trim().length > 0) {
      conditions.push('c.id = \'' + q.category + '\'');
    }

    let orderBy = '';
    if (q.sortBy) {
      if (q.sortBy === SortingMethod.RATING) {
        orderBy = 'ORDER BY b.stars DESC';
      } else if (q.sortBy === SortingMethod.REVIEWS) {
        orderBy = 'ORDER BY b.review_count DESC';
      } else if (q.sortBy === SortingMethod.DISTANCE) {
        orderBy = 'ORDER BY Distance';
      }
    }

    const returns = 'RETURN b, l, Distance, collect(c.id) as Categories';

    const commandParts: string[] = [];
    commandParts.push('MATCH (b:Business)-[:LOCATED_AT]->(l:Location)');
    commandParts.push('MATCH (b:Business)-[:BELONGS_TO]->(c:Category)');
      // tslint:disable-next-line:max-line-length
    const withStatement = 'WITH b, l, c, (round(distance(point({ longitude: l.longitude, latitude: l.latitude }), point({ longitude: ' +
      q.location.lng + ', latitude: ' + q.location.lat + ' })))/1000)*0.62137 as Distance';
    commandParts.push(withStatement);
    if (conditions.length > 0) {
      commandParts.push('WHERE ' + conditions.join(' AND '));
    }
    commandParts.push(returns);
    commandParts.push(orderBy);
    commandParts.push('LIMIT 10');
    return commandParts.join(' ');
  }

  static _parseResults(results: any): Result[] {
    return results.records.map(row => {
      const result = new Result();
      result.business = new Business(row.get(0));
      const locationJson = row.get(1);
      result.business.setLocation(locationJson);
      result.distance = row.get(2);
      result.categories = row.get(3);
      return result;
    });
  }

  public query(q: Query, successCallback?: (results: Result[]) => void) {
    const session = this.driver.session();
    const queryString = DbService._constructQuery(q);
    console.log(queryString);
    session.run(queryString).then(results => {
      const parsedResults = DbService._parseResults(results);
      console.log(parsedResults);
      successCallback(parsedResults);
    }).catch(error => {console.log(error); });
  }

  public fetchCategories(successCallback?: (categories: string[]) => void) {
    const queryString = 'MATCH (c:Category) RETURN c.id';
    const session = this.driver.session();
    session.run(queryString).then(
      results => {
        const categories = results.records.map(row => row.get(0));
        successCallback(categories);
      }
    ).catch(error => console.log(error));
  }

  public navigate(fromLoc: MapLocation, toLoc: MapLocation, successCallback: (path: RoadNode[]) => void) {
    forkJoin(
      [this.getRoadNodeForLocation(fromLoc), this.getRoadNodeForLocation(toLoc)]
    ).subscribe(data => {
      const fromRoadNode = data[0] as RoadNode;
      const toRoadNode = data[1] as RoadNode;
      this.getBestPath(fromRoadNode.id, toRoadNode.id).then(result => {
        const nodes = result.records.map(row => new RoadNode(row.get(0)));
        successCallback(nodes);
      });
    });
  }

  public getRoadNodeForLocation(location: MapLocation): any {
    const session = this.driver.session();
    console.log(location);
    const queryString = 'MATCH (n:RoadNode)\n' +
      'WITH n, distance(point({ longitude: toFloat(n.lng), latitude: toFloat(n.lat) }), ' +
      'point({ longitude: ' + location.lng + ', latitude: ' + location.lat + ' })) as Distance\n' +
      'RETURN n\n' +
      'ORDER BY Distance \n' +
      'limit 1\n';
    return session.run(queryString).then(result => new RoadNode(result.records[0].get(0)));
  }

  public getBestPath(fromNodeId: string, toNodeId: string): any {
    const session = this.driver.session();
    const queryString = 'MATCH (start:RoadNode {id: \'' + fromNodeId + '\'}), (end:RoadNode {id: \'' + toNodeId + '\'})\n' +
      'CALL gds.alpha.shortestPath.stream({\n' +
      '  nodeProjection: \'RoadNode\',\n' +
      '  relationshipProjection: {\n' +
      '    ROAD: {\n' +
      '      type: \'ROAD\',\n' +
      '      properties: \'distance\',\n' +
      '      orientation: \'UNDIRECTED\'\n' +
      '    }\n' +
      '  },\n' +
      '  startNode: start,\n' +
      '  endNode: end,\n' +
      '  relationshipWeightProperty: \'distance\'\n' +
      '})\n' +
      'YIELD nodeId, cost\n' +
      'RETURN gds.util.asNode(nodeId) AS node, cost\n';
    return session.run(queryString).then(result => result);
  }
}
