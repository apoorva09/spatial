import { Injectable } from '@angular/core';
import {Business} from '../models/business';
import {Subject} from 'rxjs';
import {DbService} from './db.service';
import {Query} from '../models/query';
import {Result} from '../models/result';
import {MapLocation} from '../models/map-location';
import {RoadNode} from '../models/road-node';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private results: Result[] = [];
  private resultsSubject = new Subject<Result[]>();
  public resultsList$ = this.resultsSubject.asObservable();

  private queryLocation: MapLocation;
  private queryLocationSubject = new Subject<MapLocation>();
  public queryLocation$ = this.queryLocationSubject.asObservable();

  private path: RoadNode[];
  private pathSubject = new Subject<RoadNode[]>();
  public path$ = this.pathSubject.asObservable();

  constructor(private dbService: DbService) {
    this.resultsSubject.next(this.results);
  }

  public runSearchQuery(query: Query, onSuccess?: () => void): void {
    // Extract location from query and broadcast.
    this.queryLocation = query.location;
    this.queryLocationSubject.next(this.queryLocation);
    // Nullify the path.
    this.path = null;
    this.pathSubject.next(this.path);

    this.dbService.query(query, (results: Result[]) => {
      this.results = results;
      this.resultsSubject.next(this.results);  // Announce the new results!
      if (onSuccess) {
        onSuccess();
      }
    });
  }

  public runNavigationQuery(navigateTo: Result): void {
    this.dbService.navigate(this.queryLocation, navigateTo.business.location, (path: RoadNode[]) => {
      this.path = path;
      this.pathSubject.next(this.path);
    });
  }

  public getResults() {
    return this.results;
  }

  public getQueryLocation() {
    return this.queryLocation;
  }

  public getPath() {
    return this.path;
  }

  public fetchCategories(onFetchSuccess: (categories: string[]) => void): void {
    this.dbService.fetchCategories(onFetchSuccess);
  }
}
