import {MapLocation} from './map-location';

export enum SortingMethod {
  RATING = 'rating',
  REVIEWS = 'reviews',
  DISTANCE = 'distance'
}

export class Query {
  location: MapLocation;
  nameTokens: string[];
  category: string;
  priceRanges: number[];
  minRating: number;
  maxDistance: number;
  takeOut: boolean;
  wifi: boolean;
  goodForGroups: boolean;
  sortBy: SortingMethod;

  constructor() {}
}
