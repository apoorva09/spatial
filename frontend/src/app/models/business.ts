import {Integer} from 'neo4j-driver';
import {MapLocation} from './map-location';


export class Business {
  address: string;
  name: string;
  isOpen: boolean;
  location: MapLocation;
  postalCode: number;
  stars: number;
  reviewCount: number;

  // Properties.
  ambience: string;
  goodForGroups: boolean;
  priceRange: number;
  takeOut: boolean;
  wifi: boolean;

  constructor(jsonObj?: any) {
    if (jsonObj != null) {
      this.address = jsonObj.properties.address;
      this.name = jsonObj.properties.name;
      this.isOpen = (jsonObj.properties.is_open as Integer).equals(1);
      // this.location = new MapLocation(jsonObj.properties.latitude, jsonObj.properties.longitude);
      this.postalCode = parseInt(jsonObj.properties.postal_code, 10);
      this.stars = parseFloat(jsonObj.properties.stars);
      this.reviewCount = (jsonObj.properties.review_count as Integer).toInt();

      if (jsonObj.properties.ambience != null && jsonObj.properties.ambience !== 'unknown') {
        // Let's improve the json object.
        const json = jsonObj.properties.ambience
          .replace(/'/g, '\"')
          .replace(/True/g, 'true')
          .replace(/False/g, 'false');
        this.ambience = JSON.parse(json);
      }
      if (jsonObj.properties.restaurantGoodForGroups != null && jsonObj.properties.restaurantGoodForGroups !== 'unknown') {
        this.goodForGroups = jsonObj.properties.restaurantGoodForGroups === 'True';
      }
      if (jsonObj.properties.restaurantPriceRange != null && jsonObj.properties.restaurantPriceRange !== 'unknown') {
        this.priceRange = parseInt(jsonObj.properties.restaurantPriceRange, 10);
      }
      if (jsonObj.properties.takeOut != null && jsonObj.properties.takeOut !== 'unknown') {
        this.takeOut = jsonObj.properties.takeOut === 'True';
      }
      if (jsonObj.properties.wifi != null && jsonObj.properties.wifi !== 'unknown') {
        this.wifi = jsonObj.properties.wifi === 'u\'free\'';
      }
    }
  }

  public setLocation(locationJson: any): void {
    if (!locationJson) {
      return;
    }
    this.location = new MapLocation(locationJson.properties.latitude, locationJson.properties.longitude);
  }
}
