
export class RoadNode {
  id: string;
  lat: number;
  lng: number;

  public constructor(obj?: any) {
    this.id = obj && obj.properties.id || null;
    this.lat = obj && parseFloat(obj.properties.lat) || null;
    this.lng = obj && parseFloat(obj.properties.lng) || null;
  }
}
