import {Business} from './business';

export class Result {
  business: Business;  // Should always be there.
  distance: number;    // Might be there, if distance was a search criteria or a sorting criteria.
  categories: string[];
}
