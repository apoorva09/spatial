import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { SearchWidgetComponent } from './components/search-widget/search-widget.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectLocationModalComponent } from './components/select-location-modal/select-location-modal.component';
import {AgmCoreModule} from '@agm/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ResultsListComponent } from './components/results-list/results-list.component';
import { ResultsMapComponent } from './components/results-map/results-map.component';
import { ResultInListComponent } from './components/result-in-list/result-in-list.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import {MatSelectModule} from '@angular/material/select';
import { DistancePipe } from './components/result-in-list/distance.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchWidgetComponent,
    SearchFormComponent,
    SearchResultsComponent,
    SelectLocationModalComponent,
    ResultsListComponent,
    ResultsMapComponent,
    ResultInListComponent,
    StarRatingComponent,
    DistancePipe,
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    NgbModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC58i2qgdZxgJo-ouognkxiZziC4txuaHc'
    }),
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
