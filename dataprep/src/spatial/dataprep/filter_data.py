import json
import operator

CITY_NAME = 'Las Vegas'
#CITY_NAME = 'Los Angeles'


PATH = '../yelp-dataset/'
original_file = PATH + 'yelp_academic_dataset_business.json'
#original_file = 'test.json'
modified_file = 'filtered_business.json'

filenames = {"yelp_academic_dataset_review.json":"review.json","yelp_academic_dataset_tip.json":"tip.json",
"yelp_academic_dataset_checkin.json":"checkin.json"}


print 'Reading business json file:'
fp = open(original_file)
mf = open(modified_file, 'w')
line = fp.readline()
business_id = {}
ctr = 0
while line:
    line = fp.readline()
    try:
        business = json.loads(line)
        if business["city"] == CITY_NAME:
	   ctr += 1  
           business_id[business["business_id"]] = True
	   # Expand attributes we like too.
	   attrs = {
	   }
	   map = {	
		'restaurantTakeOut': 'RestaurantsTakeOut',
		'wifi': 'WiFi',
		'restaurantGoodForGroups': 'RestaurantsGoodForGroups',
		'ambience': 'Ambience',
		'noiseLevel': 'NoiseLevel',
		'priceRange': 'RestaurantsPriceRange2'
	   }
	   for field in map:
		attribField = map[field]
		attrs[field] = 'unknown'
		if 'attributes' in business:
			if business['attributes'] and attribField in business['attributes']:
				attrs[field] = business['attributes'][attribField]
	   business['attrib'] = attrs
	   if 'attributes' in business:
	   	del business['attributes']
           mf.write(json.dumps(business))
	   mf.write('\n')
		
    except Exception as e:
        print line
	print e
fp.close()
mf.close()

print 'Businesses found: %d' % ctr

"""
for filename in filenames:
    print 'Generating %s' % filenames[filename]
    file = open(PATH + filename)
    new_file = open(filenames[filename],'w')
    line = file.readline()
    while line:
	try:
	    obj = json.loads(line)
            if obj["business_id"] in business_id:
		if filenames[filename] == "review.json":
			del obj['text']
		new_file.write(json.dumps(obj))
                new_file.write('\n')
	except Exception as e:
	    print(e)
	line = file.readline()
    file.close()    
    new_file.close()
"""
