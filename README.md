# Installation Instructions

## Setup

### Create a directory under which code would be cloned.

`mkdir -p ~/spatialproj`

`cd ~/spatialproj`

### Clone the code repository.

`git clone https://bitbucket.org/apoorva09/spatial.git .`

`cd frontend/`

### Make sure 'ng' is installed

Install nodejs (if needed).

`brew install node`

Install Angular CLI.
Note that version 9.1.1 was used for development.

`npm install -g @angular/cli`

Make sure angular-devkit is installed.

`npm install --save-dev @angular-devkit/build-angular`

## Running the server

`cd ~/spatialproj/frontend/`

`ng serve`

It takes a while to compile all files. Once you see "Compiled successfully", the server is up and running.

Now, navigate to http://localhost:4200 to go to the service.

That's it!
