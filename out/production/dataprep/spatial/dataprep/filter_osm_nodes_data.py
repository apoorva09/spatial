import csv

import xml.etree.ElementTree as ET


def _parse_nodes_xml(writer):
    root = ET.parse('data/las-vegas-nodes.xml').getroot()
    for node in root.findall('node'):
        writer.writerow({
            'id': node.get('id'),
            'lat': node.get('lat'),
            'lon': node.get('lon')})


def main():
    with open('data/nodes.csv', mode='w') as csv_file:
        fieldnames = ['id', 'lat', 'lon']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        _parse_nodes_xml(writer)


if __name__ == "__main__":
    main()
