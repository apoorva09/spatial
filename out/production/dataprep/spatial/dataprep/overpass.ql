# Overpass queries.
# Run on overpass-turbo.de
way[highway]({{bbox}});
node(w);  // When dumping nodes .. otherwise just remove.
out;

# After copying, EXPORT > raw data directly from overpass API.
