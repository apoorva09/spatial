import csv

import xml.etree.ElementTree as ET


def _parse_ways_xml(writer):
    root = ET.parse('data/las-vegas-ways.xml').getroot()
    for way in root.findall('way'):
        prev = None
        for node in way.findall('nd'):
            if prev is not None:
                writer.writerow({
                    'node1': node.get('ref'),
                    'node2': prev.get('ref')})
            prev = node


def main():
    with open('data/ways.csv', mode='w') as csv_file:
        fieldnames = ['node1', 'node2']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        _parse_ways_xml(writer)


if __name__ == "__main__":
  main()
